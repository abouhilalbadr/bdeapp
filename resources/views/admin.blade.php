@extends('layouts.app')

@section('title', 'Reqlab Dashboard')
@section('content')
        <div class="flex flex-row items-stretch min-h-screen">
            <div class="w-1/12 p-4 bg-white fixed h-screen">
                <div class="logo">
                    <img class="block w-24 h-24 mx-auto mb-8" src="{{ asset('images/logo-tile.svg') }}">
                </div>
                <form class="hidden" id="logout-form" action="{{ route('logout') }}" method="POST">@csrf</form>
                <side-nav logout="{{ route('logout') }}" isadmin="true"></side-nav>
            </div>
            <div class="w-11/12 bg-cleanGray maingrid">
                @if(session()->has('message'))
                    <div class="bg-green-100 border-l-4 border-green-500 text-green-700 p-4 mb-4 text-2xl" role="alert">
                        {{ session()->get('message') }}
                    </div>
                @endif
                <div class="p-10 flex flex-row -mx-2">
                    <div class="w-1/2 px-2">
                        <h2 class="font-bold px-8 py-4 text-gray-600 border-b-2 uppercase bg-white">Clubs</h2>
                        <table class="w-full flex flex-row flex-no-wrap sm:bg-white overflow-hidden sm:shadow-lg mb-5 text-center">
                            <thead class="text-white">
                            @foreach($clubs as $club)
                                <tr class="bg-mainBlue flex flex-col flex-no wrap sm:table-row  mb-2 sm:mb-0">
                                    <th class="p-3">Name</th>
                                    <th class="p-3">Description</th>
                                    <th class="p-3">Actions</th>
                                </tr>
                            @endforeach
                            </thead>
                            <tbody class="flex-1 sm:flex-none">
                            @forelse($clubs as $club)
                                <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                    <td class="border-grey-light border hover:bg-gray-100 p-3 uppercase">{{$club->name}}</td>
                                    <td class="border-grey-light border hover:bg-gray-100 p-3">{{$club->description}}</td>
                                    <td class="border-grey-light border hover:bg-gray-100 p-3 text-white hover:font-medium cursor-pointer">
                                        <a href="/admin/clubs/{{$club->id}}/edit" class="bg-green-600 py-2 px-6 inline-block hint--top hint--success" aria-label="Edit">
                                            <svg viewBox="0 0 24 24" width="16px" height="16px" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg>
                                        </a>
                                        <form action="/admin/clubs/{{$club->id}}" method="post" class="inline-block relative" style="top: -8px;">
                                            @csrf
                                            @method('DELETE')
                                            <button class="bg-red-600 py-2 px-6 focus:outline-none deleteClub hint--top hint--error" data-confirm="Are you sure you want to delete {{$club->name}} ?" aria-label="Delete">
                                                <svg fill="#fff" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16px" height="16px"><path d="M 2.75 2.042969 L 2.042969 2.75 L 2.398438 3.101563 L 7.292969 8 L 2.042969 13.25 L 2.75 13.957031 L 8 8.707031 L 12.894531 13.605469 L 13.25 13.957031 L 13.957031 13.25 L 13.605469 12.894531 L 8.707031 8 L 13.957031 2.75 L 13.25 2.042969 L 8 7.292969 L 3.101563 2.398438 Z"/></svg>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <h2 class="font-bold text-2xl px-8 py-4 bg-white">No Clubs Yet</h2>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    <div class="w-1/2 px-2">
                        <h2 class="font-bold px-8 py-4 text-gray-600 border-b-2 uppercase bg-white">Leaders</h2>
                        <table class="w-full flex flex-row flex-no-wrap sm:bg-white overflow-hidden sm:shadow-lg mb-5 text-center">
                            <thead class="text-white">
                            @foreach($users as $user)
                                <tr class="bg-mainBlue flex flex-col flex-no wrap sm:table-row  mb-2 sm:mb-0">
                                    <th class="p-3">Name</th>
                                    <th class="p-3">Email</th>
                                    <th class="p-3">Club</th>
                                    <th class="p-3">Actions</th>
                                </tr>
                            @endforeach
                            </thead>
                            <tbody class="flex-1 sm:flex-none">
                            @forelse($users as $user)
                                <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                    <td class="border-grey-light border hover:bg-gray-100 p-3 uppercase">{{$user->name}}</td>
                                    <td class="border-grey-light border hover:bg-gray-100 p-3">{{$user->email}}</td>
                                    <td class="border-grey-light border hover:bg-gray-100 p-3 capitalize">
                                        @foreach($clubs as $club)
                                            @if($club->id == $user->club_id)
                                                {{$club->name}}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td class="border-grey-light border hover:bg-gray-100 p-3 text-white hover:font-medium cursor-pointer">
                                        <a href="/admin/leaders/{{$user->id}}/edit" class="bg-green-600 py-2 px-6 inline-block hint--top hint--success" aria-label="Edit">
                                            <svg viewBox="0 0 24 24" width="16px" height="16px" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg>
                                        </a>
                                        <form action="/admin/leaders/{{$user->id}}" method="post" class="inline-block relative" style="top: -8px;">
                                            @csrf
                                            @method('DELETE')
                                            <button class="bg-red-600 py-2 px-6 focus:outline-none deleteClub hint--top hint--error" data-confirm="Are you sure you want to delete {{$user->name}} ?" aria-label="Delete">
                                                <svg fill="#fff" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16px" height="16px"><path d="M 2.75 2.042969 L 2.042969 2.75 L 2.398438 3.101563 L 7.292969 8 L 2.042969 13.25 L 2.75 13.957031 L 8 8.707031 L 12.894531 13.605469 L 13.25 13.957031 L 13.957031 13.25 L 13.605469 12.894531 L 8.707031 8 L 13.957031 2.75 L 13.25 2.042969 L 8 7.292969 L 3.101563 2.398438 Z"/></svg>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <h2 class="font-bold text-2xl px-8 py-4 bg-white">No Leaders Yet</h2>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="flex flex-row p-10">
                    <div class="w-full">
                        <h2 class="font-bold px-8 py-4 text-gray-600 border-b-2 uppercase bg-white">Pending Requests</h2>
                        <table class="w-full flex flex-row flex-no-wrap sm:bg-white overflow-hidden sm:shadow-lg mb-5 text-center">
                            <thead class="text-white">
                            @foreach($reqs as $req)
                                <tr class="bg-mainBlue flex flex-col flex-no wrap sm:table-row  mb-2 sm:mb-0">
                                    <th class="p-3">Leader</th>
                                    <th class="p-3">Club</th>
                                    <th class="p-3">Title</th>
                                    <th class="p-3">Description</th>
                                    <th class="p-3">Priority</th>
                                    <th class="p-3">Status</th>
                                    <th class="p-3">Actions</th>
                                </tr>
                            @endforeach
                            </thead>
                            <tbody class="flex-1 sm:flex-none">
                            @forelse($reqs as $req)
                                <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                    @foreach($users as $user)
                                        @if($user->id == $req->user_id)
                                            <td class="border-grey-light border hover:bg-gray-100 p-3 uppercase">{{$user->name}}</td>
                                            <td class="border-grey-light border hover:bg-gray-100 p-3 uppercase">
                                                @foreach($clubs as $club)
                                                    @if($user->club_id == $club->id)
                                                        {{$club->name}}
                                                    @endif
                                                @endforeach
                                            </td>
                                        @endif
                                    @endforeach
                                    <td class="border-grey-light border hover:bg-gray-100 p-3 uppercase">{{$req->title}}</td>
                                    <td class="border-grey-light border hover:bg-gray-100 p-3">{{$req->description}}</td>
                                    <td class="border-grey-light border hover:bg-gray-100 p-3">
                                        @if($req->priority == 0)
                                            Not Important
                                        @elseif($req->priority == 1)
                                            Important
                                        @elseif($req->priority == 2)
                                            Urgent
                                        @endif
                                    </td>
                                    <td class="border-grey-light border hover:bg-gray-100 p-3">
                                        @if($req->status == 2)
                                            <span class="bg-green-500 rounded-full text-white text-sm px-4 py-1">Approved</span>
                                        @elseif($req->status == 1)
                                            <span class="bg-red-500 rounded-full text-white text-sm px-4 py-1">Not Approved</span>
                                        @else
                                            <span class="bg-yellow-500 rounded-full text-white text-sm px-4 py-1">Pending</span>
                                        @endif
                                    </td>
                                    <td class="border-grey-light border hover:bg-gray-100 p-3 text-white hover:font-medium cursor-pointer">
                                        <form action="/admin/requests/{{$req->id}}" method="post" class="inline-block relative">
                                            @csrf
                                            @method('PATCH')
                                            <button class="bg-green-600 py-2 px-6 focus:outline-none hint--top hint--success" aria-label="Approve">
                                                <svg viewBox="0 0 24 24" width="16px" height="16px" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round"><polyline points="20 6 9 17 4 12"></polyline></svg>
                                            </button>
                                        </form>
                                        <form action="/admin/requests/{{$req->id}}" method="post" class="inline-block relative">
                                            @csrf
                                            @method('DELETE')
                                            <button class="bg-red-600 py-2 px-6 focus:outline-none deleteClub hint--top hint--error" data-confirm="Are you sure you want to Disapproved {{$req->title}} ?" aria-label="Disapproved">
                                                <svg fill="#fff" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16px" height="16px"><path d="M 2.75 2.042969 L 2.042969 2.75 L 2.398438 3.101563 L 7.292969 8 L 2.042969 13.25 L 2.75 13.957031 L 8 8.707031 L 12.894531 13.605469 L 13.25 13.957031 L 13.957031 13.25 L 13.605469 12.894531 L 8.707031 8 L 13.957031 2.75 L 13.25 2.042969 L 8 7.292969 L 3.101563 2.398438 Z"/></svg>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <h2 class="font-bold text-2xl px-8 py-4 bg-white">No Requests Yet</h2>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="p-10 flex flex-row -mx-2">
                    <div class="w-1/2 px-2">
                        <h2 class="font-bold text-2xl px-8 py-4 text-gray-600 border-b-2 uppercase bg-white">Approved Requests</h2>
                        <table class="w-full flex flex-row flex-no-wrap sm:bg-white overflow-hidden sm:shadow-lg mb-5 text-center">
                            <thead class="text-white">
                            @foreach($appreqs as $req)
                                <tr class="bg-mainBlue flex flex-col flex-no wrap sm:table-row  mb-2 sm:mb-0">
                                    <th class="p-3">Title</th>
                                    <th class="p-3">Description</th>
                                    <th class="p-3">Priority</th>
                                    <th class="p-3">Status</th>
                                </tr>
                            @endforeach
                            </thead>
                            <tbody class="flex-1 sm:flex-none">
                            @forelse($appreqs as $req)
                                <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                    <td class="border-grey-light border hover:bg-gray-100 p-3 uppercase">{{$req->title}}</td>
                                    <td class="border-grey-light border hover:bg-gray-100 p-3">{{$req->description}}</td>
                                    <td class="border-grey-light border hover:bg-gray-100 p-3">
                                        @if($req->priority == 0)
                                            Not Important
                                        @elseif($req->priority == 1)
                                            Important
                                        @elseif($req->priority == 2)
                                            Urgent
                                        @endif
                                    </td>
                                    <td class="border-grey-light border hover:bg-gray-100 p-3">
                                        <span class="bg-green-500 rounded-full text-white text-sm px-4 py-1">Approved</span>
                                    </td>

                                </tr>
                            @empty
                                <h2 class="font-bold text-2xl px-8 py-4 bg-white">No Approved Request</h2>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    <div class="w-1/2 px-2">
                        <h2 class="font-bold text-2xl px-8 py-4 text-gray-600 border-b-2 uppercase bg-white">Disapproved Requests</h2>
                        <table class="w-full flex flex-row flex-no-wrap sm:bg-white overflow-hidden sm:shadow-lg mb-5 text-center">
                            <thead class="text-white">
                            @foreach($disapreq as $req)
                                <tr class="bg-mainBlue flex flex-col flex-no wrap sm:table-row  mb-2 sm:mb-0">
                                    <th class="p-3">Title</th>
                                    <th class="p-3">Description</th>
                                    <th class="p-3">Priority</th>
                                    <th class="p-3">Status</th>
                                </tr>
                            @endforeach
                            </thead>
                            <tbody class="flex-1 sm:flex-none">
                            @forelse($disapreq as $req)
                                <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                    <td class="border-grey-light border hover:bg-gray-100 p-3 uppercase">{{$req->title}}</td>
                                    <td class="border-grey-light border hover:bg-gray-100 p-3">{{$req->description}}</td>
                                    <td class="border-grey-light border hover:bg-gray-100 p-3">
                                        @if($req->priority == 0)
                                            Not Important
                                        @elseif($req->priority == 1)
                                            Important
                                        @elseif($req->priority == 2)
                                            Urgent
                                        @endif
                                    </td>
                                    <td class="border-grey-light border hover:bg-gray-100 p-3">
                                        <span class="bg-red-500 rounded-full text-white text-sm px-4 py-1">Disapproved</span>
                                    </td>

                                </tr>
                            @empty
                                <h2 class="font-bold text-2xl px-8 py-4 bg-white">No Disapproved Request</h2>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
@endsection
