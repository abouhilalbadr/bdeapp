@component('mail::message')
# Welcome to {{ config('app.name') }}

You can login with your email : {{$data['email']}}

And your Password is : {{$data['password']}}

@component('mail::button', ['url' => '', 'color' => 'success'])
Sign in
@endcomponent

Thanks, {{ config('app.name') }}
@endcomponent
