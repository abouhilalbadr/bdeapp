@extends('layouts.app')

@section('title', 'Reqlab Profile')

@section('content')
    <div class="flex flex-row items-stretch min-h-screen">
        <div class="w-1/12 p-4 bg-white fixed h-screen">
            <div class="logo">
                <img class="block w-24 h-24 mx-auto mb-8" src="{{ asset('images/logo-tile.svg') }}">
            </div>
            <form class="hidden" id="logout-form" action="{{ route('logout') }}" method="POST">@csrf</form>
            <side-nav logout="{{ route('logout') }}"  isadmin="true"></side-nav>
        </div>
        <div class="w-11/12 bg-cleanGray maingrid">
            @if ($errors->any())
                <div class="bg-red-100 border-l-4 border-red-500 text-red-700 p-4 mb-4" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li class="font-bold">{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session()->has('message'))
                <div class="bg-green-100 border-l-4 border-green-500 text-green-700 p-4 mb-4" role="alert">
                    {{ session()->get('message') }}
                </div>
            @endif
            <div class="flex flex-row p-10">
                <div class="w-full bg-white relative">
                    <h2 class="text-4xl font-bold px-8 py-4 text-gray-600 border-b-2 uppercase">Your profile</h2>
                    <a href="/admin/profile/{{auth()->user()->id}}/edit" class="absolute top-0 right-0 m-8 hint--left hint--info" aria-label="Edit Profile" style="position: absolute">
                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="inline-block"><circle cx="12" cy="12" r="3"></circle><path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path></svg>
                    </a>
                    <div class="flex flex-row p-8">
                        <div class="w-full lg:w-1/2">
                            <h4 class="text-gray-700 font-bold text-2xl">Username</h4>
                            <p class="text-gray-500 text-xl">{{auth()->user()->name}}</p>
                        </div>
                        <div class="w-full lg:w-1/2">
                            <h4 class="text-gray-700 font-bold text-2xl">Email</h4>
                            <p class="text-gray-500 text-xl">{{auth()->user()->email}}</p>
                        </div>
                    </div>
                    <div class="flex flex-row p-8">
                        <div class="w-full lg:w-1/2">
                            <h4 class="text-gray-700 font-bold text-2xl">Role</h4>
                            <p class="text-gray-500 text-xl uppercase">Admin</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
