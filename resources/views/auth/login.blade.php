@extends('layouts.app')

@section('title', 'Login')

@section('content')
    <div class="w-full h-screen relative">
        <video class="hidden lg:block pointer-events-none w-full h-full object-cover" autoplay disablepictureinpicture="true" loop muted playsinline="true" src="{{ asset('videos/login.mp4') }}"></video>
        @isset($url)
            <form class="loginForm relative lg:absolute" method="POST" action='{{ url("login/$url") }}' aria-label="{{ __('Login') }}">
        @else
            <form class="loginForm relative lg:absolute" method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
        @endisset
            @csrf
            <img class="block mx-auto mb-8" src="{{ asset('images/logo-dark.png') }}">
            <div class="relative mb-8">
                <label class="absolute block text-center rounded-l label" for="username">
                    <svg viewBox="0 0 24 24" width="24" height="24" stroke="#fff" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="inline-block"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>
                </label>
                <input id="email" type="email" class="w-full rounded input focus:outline-none @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="off" autofocus placeholder="Email">

            </div>
            <div class="relative mb-8">
                <label class="absolute block text-center rounded-l label" for="password">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="inline-block"><path d="M21 2l-2 2m-7.61 7.61a5.5 5.5 0 1 1-7.778 7.778 5.5 5.5 0 0 1 7.777-7.777zm0 0L15.5 7.5m0 0l3 3L22 7l-3-3m-3.5 3.5L19 4"></path></svg>
                </label>
                <input id="password" type="password" class="w-full rounded input focus:outline-none @error('password') is-invalid @enderror" name="password" required autocomplete="off" placeholder="Password">
            </div>
            @if ($errors->any())
                <div class="bg-red-100 border-l-4 border-red-500 text-red-700 p-4 mb-4" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li class="font-bold">{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="relative mb-8">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                    <label class="form-check-label ml-2 text-white" for="remember">
                        {{ __('Remember Me') }}
                    </label>
                </div>
            </div>
            <div class="relative">
                <button type="submit" class="btn-submit bg-mainBlue block text-white py-2 px-6 w-full rounded">
                    {{ __('Login') }}
                </button>

                @if (Route::has('password.request'))
                    <a class="block my-4 text-center text-white" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                @endif
            </div>
        </form>
    </div>
@endsection
