@extends('layouts.app')

@section('title', 'Reqlab Request')
@section('content')
    <div class="flex flex-row items-stretch min-h-screen">
        <div class="w-1/12 p-4 bg-white fixed h-screen">
            <div class="logo">
                <img class="block w-24 h-24 mx-auto mb-8" src="{{ asset('images/logo-tile.svg') }}">
            </div>
            <form class="hidden" id="logout-form" action="{{ route('logout') }}" method="POST">@csrf</form>
            <side-nav logout="{{ route('logout') }}" isadmin="false"></side-nav>
        </div>
        <div class="w-11/12 bg-cleanGray maingrid">
            <div class="relative m-10">
                <button id="showClubForm" class="rounded-full w-16 h-16 bg-white shadow hover:shadow-2xl transition focus:outline-none absolute right-15 top-15 hint--left hint--info" aria-label="Add Request" style="position: absolute">
                    <svg viewBox="0 0 24 24" width="24" height="24" stroke="#2649f1" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="inline-block"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg>
                </button>
                <h2 class="text-6xl font-bold uppercase">Requests</h2>
                @if ($errors->any())
                    <div class="bg-red-100 border-l-4 border-red-500 text-red-700 p-4 mb-4" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li class="font-bold">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if(session()->has('message'))
                    <div class="bg-green-100 border-l-4 border-green-500 text-green-700 p-4 mb-4" role="alert">
                        {{ session()->get('message') }}
                    </div>
                @endif
                <div class="py-10 flex flex-row">
                    <div class="w-full">
                        <table class="w-full flex flex-row flex-no-wrap sm:bg-white overflow-hidden sm:shadow-lg mb-5 text-center">
                            <thead class="text-white">
                            @foreach($reqs as $req)
                                <tr class="bg-mainBlue flex flex-col flex-no wrap sm:table-row  mb-2 sm:mb-0">
                                    <th class="p-3">Title</th>
                                    <th class="p-3">Description</th>
                                    <th class="p-3">Priority</th>
                                    <th class="p-3">Status</th>
                                    @if($req->status == 0)
                                        <th class="p-3">Actions</th>
                                    @endif
                                </tr>
                            @endforeach
                            </thead>
                            <tbody class="flex-1 sm:flex-none">
                            @forelse($reqs as $req)
                                <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                    <td class="border-grey-light border hover:bg-gray-100 p-3 uppercase">{{$req->title}}</td>
                                    <td class="border-grey-light border hover:bg-gray-100 p-3">{{$req->description}}</td>
                                    <td class="border-grey-light border hover:bg-gray-100 p-3">
                                        @if($req->priority == 0)
                                            Not Important
                                        @elseif($req->priority == 1)
                                            Important
                                        @elseif($req->priority == 2)
                                            Urgent
                                        @endif
                                    </td>
                                    <td class="border-grey-light border hover:bg-gray-100 p-3">
                                        @if($req->status == 2)
                                            <span class="bg-green-500 rounded-full text-white text-sm px-4 py-1">Approved</span>
                                        @elseif($req->status == 1)
                                            <span class="bg-red-500 rounded-full text-white text-sm px-4 py-1">Not Approved</span>
                                        @else
                                            <span class="bg-yellow-500 rounded-full text-white text-sm px-4 py-1">Pending</span>
                                        @endif
                                    </td>
                                    <td class="border-grey-light border hover:bg-gray-100 p-3 text-white hover:font-medium cursor-pointer">
                                        @if($req->status == 0)
                                            <a href="/home/requests/{{$req->id}}/edit" class="bg-green-600 py-2 px-6 inline-block hint--success hint--top" aria-label="Edit">
                                                <svg viewBox="0 0 24 24" width="16px" height="16px" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg>
                                            </a>
                                            <form action="/home/requests/{{$req->id}}" method="post" class="inline-block relative" style="top: -8px;">
                                                @csrf
                                                @method('DELETE')
                                                <button class="bg-red-600 py-2 px-6 focus:outline-none deleteClub hint--top hint--error" aria-label="Delete" data-confirm='Are you sure you want to delete "{{$req->title}}" ?'>
                                                    <svg fill="#fff" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16px" height="16px"><path d="M 2.75 2.042969 L 2.042969 2.75 L 2.398438 3.101563 L 7.292969 8 L 2.042969 13.25 L 2.75 13.957031 L 8 8.707031 L 12.894531 13.605469 L 13.25 13.957031 L 13.957031 13.25 L 13.605469 12.894531 L 8.707031 8 L 13.957031 2.75 L 13.25 2.042969 L 8 7.292969 L 3.101563 2.398438 Z"/></svg>
                                                </button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                <div class="bg-orange-100 border-l-4 border-orange-500 text-orange-700 p-4" role="alert">
                                    <p class="font-bold tet-xl">Be Warned</p>
                                    <p class="text-l">No Pending Request.</p>
                                </div>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="clubStore" class="hidden">
        <div class="fixed w-full h-full top-0 left-0 flex items-center justify-center z-10 opacity-100 pointer-events-auto">
            <div class="absolute w-full h-full bg-gray-900 opacity-75"></div>
            <div class="bg-white w-11/12 md:w-1/3 mx-auto rounded shadow-lg z-50 overflow-y-auto">
                <div class="modal-content text-left">
                    <div class="flex justify-between items-center p-3 border-b">
                        <p class="text-3xl font-bold uppercase">Send a Request</p>
                        <button id="hideClubForm" class="cursor-pointer z-50 focus:outline-none">
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18" class="fill-current text-black">
                                <path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"></path>
                            </svg>
                        </button>
                    </div>
                    <form class="p-4" action="/home/requests" method="post">
                        @csrf
                        <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
                        <input type="hidden" name="status" value="0">
                        <div class="text-base mb-4">
                            <label for="title" class="text-l font-bold block mb-4">Object Name</label>
                            <input id="title" autocomplete="off" name="title" class="w-full h-12 p-4 focus:outline-none border focus:border-mainBlue rounded" required>
                        </div>
                        <div class="text-base mb-4">
                            <label for="desc" class="text-l font-bold block mb-4">Description</label>
                            <textarea id="desc" autocomplete="off" name="description" class="w-full h-48 p-4 focus:outline-none border focus:border-mainBlue rounded" required></textarea>
                        </div>
                        <div class="text-base mb-4">
                            <label for="prio" class="text-l font-bold block mb-4">Priority</label>
                            <select name="priority" id="prio" class="w-full h-12 p-4 focus:outline-none border focus:border-mainBlue rounded">
                                <option value="0">Not Important</option>
                                <option value="1">Important</option>
                                <option value="2">Urgent</option>
                            </select>
                        </div>
                        <button type="submit" class="bg-mainBlue hover:bg-secondBlue text-white py-2 px-10 rounded focus:outline-none transition">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
