@extends('layouts.app')

@section('title', 'Reqlab Request')
@section('content')
    <div class="flex flex-row items-stretch min-h-screen">
        <div class="w-1/12 p-4 bg-white fixed h-screen">
            <div class="logo">
                <img class="block w-24 h-24 mx-auto mb-8" src="{{ asset('images/logo-tile.svg') }}">
            </div>
            <form class="hidden" id="logout-form" action="{{ route('logout') }}" method="POST">@csrf</form>
            <side-nav logout="{{ route('logout') }}" isadmin="false"></side-nav>
        </div>
        <div class="w-11/12 bg-cleanGray maingrid">
            <div class="relative m-10">
                <h2 class="text-6xl font-bold uppercase">edit Request</h2>
                <div class="py-10 flex flex-row">
                    <div class="w-full">
                        <form class="p-4" action="/home/requests/{{$reqid->id}}" method="post">
                            @csrf
                            @method('PATCH')
                            <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
                            <input type="hidden" name="status" value="0">
                            <div class="text-base mb-4">
                                <label for="title" class="text-l font-bold block mb-4">Object Name</label>
                                <input id="title" autocomplete="off" name="title" class="w-full h-12 p-4 focus:outline-none border focus:border-mainBlue rounded" value="{{$reqid->title}}" required>
                            </div>
                            <div class="text-base mb-4">
                                <label for="desc" class="text-l font-bold block mb-4">Description</label>
                                <textarea id="desc" autocomplete="off" name="description" class="w-full h-48 p-4 focus:outline-none border focus:border-mainBlue rounded" required>{{$reqid->description}}</textarea>
                            </div>
                            <div class="text-base mb-4">
                                <label for="prio" class="text-l font-bold block mb-4">Priority</label>
                                <select name="priority" id="prio" class="w-full h-12 p-4 focus:outline-none border focus:border-mainBlue rounded">
                                    <option value="0" @if($reqid->priority == 0) selected @endif>Not Important</option>
                                    <option value="1" @if($reqid->priority == 1) selected @endif>Important</option>
                                    <option value="2" @if($reqid->priority == 2) selected @endif>Urgent</option>
                                </select>
                            </div>
                            <button type="submit" class="bg-mainBlue hover:bg-secondBlue text-white py-2 px-10 rounded focus:outline-none transition">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
