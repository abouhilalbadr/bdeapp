@extends('layouts.app')

@section('title', 'Reqlab Request')
@section('content')
    <div class="flex flex-row items-stretch min-h-screen">
        <div class="w-1/12 p-4 bg-white fixed h-screen">
            <div class="logo">
                <img class="block w-24 h-24 mx-auto mb-8" src="{{ asset('images/logo-tile.svg') }}">
            </div>
            <form class="hidden" id="logout-form" action="{{ route('logout') }}" method="POST">@csrf</form>
            <side-nav logout="{{ route('logout') }}" isadmin="true"></side-nav>
        </div>
        <div class="w-11/12 bg-cleanGray maingrid">
            <div class="relative m-10">
                <h2 class="text-6xl font-bold uppercase">Requests</h2>
                <div class="py-10 flex flex-row">
                    @if(!$users->isEmpty())
                    <div class="w-full">
                        <table class="w-full flex flex-row flex-no-wrap sm:bg-white overflow-hidden sm:shadow-lg mb-5 text-center">
                            <thead class="text-white">
                            @foreach($reqs as $req)
                                <tr class="bg-mainBlue flex flex-col flex-no wrap sm:table-row  mb-2 sm:mb-0">
                                    <th class="p-3">Leader</th>
                                    <th class="p-3">Club</th>
                                    <th class="p-3">Title</th>
                                    <th class="p-3">Description</th>
                                    <th class="p-3">Priority</th>
                                    <th class="p-3">Status</th>
                                    @if(!$req->status)
                                        <th class="p-3">Actions</th>
                                    @endif
                                </tr>
                            @endforeach
                            </thead>
                            <tbody class="flex-1 sm:flex-none">
                            @forelse($reqs as $req)
                                <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                    @foreach($users as $user)
                                        @if($user->id == $req->user_id)
                                            <td class="border-grey-light border hover:bg-gray-100 p-3 uppercase">{{$user->name}}</td>
                                            <td class="border-grey-light border hover:bg-gray-100 p-3 uppercase">
                                                @foreach($clubs as $club)
                                                    @if($user->club_id == $club->id)
                                                        {{$club->name}}
                                                    @endif
                                                @endforeach
                                            </td>
                                        @endif
                                    @endforeach
                                    <td class="border-grey-light border hover:bg-gray-100 p-3 uppercase">{{$req->title}}</td>
                                    <td class="border-grey-light border hover:bg-gray-100 p-3">{{$req->description}}</td>
                                    <td class="border-grey-light border hover:bg-gray-100 p-3">
                                        @if($req->priority == 0)
                                            Not Important
                                        @elseif($req->priority == 1)
                                            Important
                                        @elseif($req->priority == 2)
                                            Urgent
                                        @endif
                                    </td>
                                    <td class="border-grey-light border hover:bg-gray-100 p-3">
                                        @if($req->status)
                                            <span class="bg-green-500 rounded-full text-white text-sm px-4 py-1">Approved</span>
                                        @else
                                            <span class="bg-yellow-500 rounded-full text-white text-sm px-4 py-1">Pending</span>
                                        @endif
                                    </td>
                                    @if(!$req->status)
                                       <td class="border-grey-light border hover:bg-gray-100 p-3 text-white hover:font-medium cursor-pointer">
                                           <form action="/admin/requests/{{$req->id}}" method="post" class="inline-block relative">
                                               @csrf
                                               @method('PATCH')
                                               <button class="bg-green-600 py-2 px-6 focus:outline-none hint--top hint--success" aria-label="Approve">
                                                   <svg viewBox="0 0 24 24" width="16px" height="16px" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round"><polyline points="20 6 9 17 4 12"></polyline></svg>
                                               </button>
                                           </form>
                                           <form action="/admin/requests/{{$req->id}}" method="post" class="inline-block relative">
                                               @csrf
                                               @method('DELETE')
                                               <button class="bg-red-600 py-2 px-6 focus:outline-none deleteClub hint--top hint--error" data-confirm="Are you sure you want to Disapproved {{$req->title}} ?" aria-label="Disapproved">
                                                   <svg fill="#fff" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16px" height="16px"><path d="M 2.75 2.042969 L 2.042969 2.75 L 2.398438 3.101563 L 7.292969 8 L 2.042969 13.25 L 2.75 13.957031 L 8 8.707031 L 12.894531 13.605469 L 13.25 13.957031 L 13.957031 13.25 L 13.605469 12.894531 L 8.707031 8 L 13.957031 2.75 L 13.25 2.042969 L 8 7.292969 L 3.101563 2.398438 Z"/></svg>
                                               </button>
                                           </form>
                                       </td>
                                    @endif
                                </tr>
                            @empty
                                <div class="bg-orange-100 border-l-4 border-orange-500 text-orange-700 p-4" role="alert">
                                    <p class="font-bold tet-xl">Be Warned</p>
                                    <p class="text-l">No Request Yet.</p>
                                </div>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    @else
                        <div class="w-full bg-red-100 border-l-4 border-red-500 text-red-700 p-4" role="alert">
                            <p class="font-bold">Be Warned</p>
                            <p>Please create a Leader to see the requests.</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
