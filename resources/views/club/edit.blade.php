@extends('layouts.app')

@section('title', 'Reqlab Clubs')
@section('content')
    <div class="flex flex-row items-stretch min-h-screen">
        <div class="w-1/12 p-4 bg-white fixed h-screen">
            <div class="logo">
                <img class="block w-24 h-24 mx-auto mb-8" src="{{ asset('images/logo-tile.svg') }}">
            </div>
            <form class="hidden" id="logout-form" action="{{ route('logout') }}" method="POST">@csrf</form>
            <side-nav logout="{{ route('logout') }}" isadmin="true"></side-nav>
        </div>
        <div class="w-11/12 bg-cleanGray maingrid">
            <div class="relative m-10">
                <h2 class="text-6xl font-bold uppercase">edit clubs</h2>
                @if ($errors->any())
                    <div class="bg-red-100 border-l-4 border-red-500 text-red-700 p-4 mb-4" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li class="font-bold">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="py-10 flex flex-row">
                    <div class="w-full">
                        <form class="p-4" action="/admin/clubs/{{$clubid->id}}" method="post">
                            @csrf
                            @method('PATCH')
                            <input type="hidden" value="{{$clubid->id}}" name="id">
                            <div class="text-base mb-4">
                                <label for="title" class="text-l font-bold block mb-4">Name</label>
                                <input id="title" autocomplete="off" name="name" class="w-full h-12 p-4 focus:outline-none border focus:border-mainBlue rounded" value="{{$clubid->name}}" required>
                            </div>
                            <div class="text-base mb-4">
                                <label for="desc" class="text-l font-bold block mb-4">Description</label>
                                <textarea id="desc" autocomplete="off" name="description" class="w-full h-48 p-4 focus:outline-none border focus:border-mainBlue rounded" required>{{$clubid->description}}</textarea>
                            </div>
                            <button type="submit" class="bg-mainBlue hover:bg-secondBlue text-white py-2 px-10 rounded focus:outline-none transition">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
