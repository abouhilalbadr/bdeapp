/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

const Swal = require('sweetalert2');

const Darkmode = require('darkmode-js');

let options = {
    bottom: '64px', // default: '32px'
    right: '64px', // default: '32px'
    left: 'unset', // default: 'unset'
    time: '0.5s', // default: '0.3s'
    mixColor: '#fff', // default: '#fff'
    backgroundColor: '#fff',  // default: '#fff'
    buttonColorDark: '#100f2c',  // default: '#100f2c'
    buttonColorLight: '#fff', // default: '#fff'
    saveInCookies: true, // default: true,
    label: '🌓', // default: ''
    autoMatchOsTheme: true // default: true
}

const darkmode = new Darkmode(options);
darkmode.showWidget();

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('side-nav', require('./components/SideNav.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

/**
 * Select Button and do some Actions
 */

let logoutBtn = document.getElementById('submitForm') || '';
let hideClubBtn = document.getElementById('hideClubForm') || '';
let createClubBtn = document.getElementById('showClubForm') || '';
let deleteClubBtn = document.querySelectorAll('.deleteClub') || '';

for (var i = 0; i < deleteClubBtn.length; i++) {
    deleteClubBtn[i].addEventListener('click', function(event) {
        event.preventDefault();
        Swal.fire({
            title: this.getAttribute('data-confirm'),
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
                this.parentNode.submit();
            }
        })
    });
}

logoutBtn.onclick = function (e) {
    e.preventDefault();
    document.getElementById('logout-form').submit();
}

createClubBtn.onclick = function (e) {
    e.preventDefault();
    document.getElementById('clubStore').classList.remove('hidden');
}

hideClubBtn.onclick = function (e) {
    e.preventDefault();
    document.getElementById('clubStore').classList.add('hidden');
}
