<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'welcome');
Auth::routes();

Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm');
Route::get('/register/admin', 'Auth\RegisterController@showAdminRegisterForm');

Route::post('/login/admin', 'Auth\LoginController@adminLogin');
Route::post('/register/admin', 'Auth\RegisterController@createAdmin');

Route::get('/home', 'HomeController@index')->middleware('auth');
Route::get('/home/requests', 'RequestController@index')->middleware('auth');
Route::post('/home/requests', 'RequestController@store')->middleware('auth');
Route::get('/home/requests/{reqid}/edit', 'RequestController@edit')->middleware('auth');
Route::patch('/home/requests/{reqid}', 'RequestController@update')->middleware('auth');
Route::delete('/home/requests/{reqid}', 'RequestController@destroy')->middleware('auth');

Route::get('/home/profile', 'ProfileController@index')->middleware('auth');
Route::get('/home/profile/{userid}/edit', 'ProfileController@edit')->middleware('auth');
Route::patch('/home/profile/{userid}', 'ProfileController@update')->middleware('auth');


Route::get('/admin', 'AdminController@index')->middleware('auth:admin');
Route::get('/admin/profile', 'AdminController@show')->middleware('auth:admin');
Route::get('/admin/profile/{userid}/edit', 'AdminController@edit')->middleware('auth:admin');
Route::patch('/admin/profile/{userid}', 'AdminController@update')->middleware('auth:admin');

Route::get('/admin/clubs', 'ClubController@index')->middleware('auth:admin');
Route::post('/admin/clubs', 'ClubController@store')->middleware('auth:admin');
Route::get('/admin/clubs/{clubid}/edit', 'ClubController@edit')->middleware('auth:admin');
Route::patch('/admin/clubs/{clubid}', 'ClubController@update')->middleware('auth:admin');
Route::delete('/admin/clubs/{clubid}', 'ClubController@destroy')->middleware('auth:admin');

Route::get('/admin/leaders', 'UserController@index')->middleware('auth:admin');
Route::post('/admin/leaders', 'UserController@store')->middleware('auth:admin');
Route::get('/admin/leaders/{userid}/edit', 'UserController@edit')->middleware('auth:admin');
Route::patch('/admin/leaders/{userid}', 'UserController@update')->middleware('auth:admin');
Route::delete('/admin/leaders/{userid}', 'UserController@destroy')->middleware('auth:admin');

Route::get('/admin/requests', 'RequestController@adminindex')->middleware('auth:admin');
Route::patch('/admin/requests/{reqid}', 'RequestController@approve')->middleware('auth:admin');
Route::delete('/admin/requests/{reqid}', 'RequestController@notapprove')->middleware('auth:admin');


Route::get('/email', function () {
    $data['email'] = "badr@mail.com";
    $data['password'] = "2644";
   return new \App\Mail\WelcomeMail($data);
});

//Route::redirect('/register', '/login');
Route::redirect('/', '/login');
