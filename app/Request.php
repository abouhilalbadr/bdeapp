<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;


class Request extends Model
{

    protected $fillable = [
        'title', 'description', 'priority', 'status', 'user_id'
    ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
