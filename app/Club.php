<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    protected $fillable = [
        'name', 'description',
    ];
    public function club()
    {
        return $this->hasOne('App\User');
    }
}
