<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Club;
use App\User;
use App\Request;
use Illuminate\Support\Facades\App;
use App\Rules\MatchOldPassword;
use Auth;


class AdminController extends Controller
{
    /**
     * Validate a listing of the data.
     */
    protected function validateData() {

        return request()->validate([
            'name' => 'required',
            'email' => 'sometimes|required|email|unique:users,email,'.request()->id,
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => 'required|min:8',
            'new_confirm_password' => 'same:new_password',
        ]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clubs = Club::all();
        $users = User::all();
        $reqs = Request::where('status', 0)->get();
        $appreqs = Request::where('status', 2)->get();
        $disapreq = Request::where('status', 1)->get();
        return view('admin', compact('clubs', 'users', 'reqs', 'appreqs', 'disapreq'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('profile.admin');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $userid)
    {
        return view('profile.adminedit', compact('userid'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        $data = $this->validateData();

        $user = Auth::user();

        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['new_password']);

        $user->save();

        return redirect('/admin/profile')->with('message', $data['name'].' Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        //
    }
}
