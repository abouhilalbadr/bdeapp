<?php

namespace App\Http\Controllers;

use App\Club;
use App\User;
use Auth;
use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;

class ProfileController extends Controller
{
    /**
     * Validate a listing of the data.
     */
    protected function validateData() {

        return request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => 'required|min:8',
            'new_confirm_password' => 'same:new_password',
        ]);

    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $clubid = auth()->user()->club_id;
        $club = Club::where('id', $clubid)->first();
        return view('profile.index', compact('club'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $userid)
    {
        return view('profile.edit', compact('userid'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        $data = $this->validateData();
        $clubid = auth()->user()->club_id;

        $user = Auth::user();

        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['new_password']);
        $user->club_id = $clubid;

        $user->save();

        return redirect('/home/profile')->with('message', $data['name'].' Updated Successfully');
    }
}
