<?php

namespace App\Http\Controllers;

use App\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userid = auth()->user()->id;
        $reqs = Request::where('user_id', $userid)->where('status', 0)->get();
        $appreqs = Request::where('user_id', $userid)->where('status', 2)->get();
        $disapreq = Request::where('user_id', $userid)->where('status', 1)->get();
        return view('home', compact('reqs', 'appreqs', 'disapreq'));
    }
}
