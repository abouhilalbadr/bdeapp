<?php

namespace App\Http\Controllers;

use App\User;
use App\Club;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use \App\Mail\WelcomeMail;

class UserController extends Controller
{
    /**
     * Validate a listing of the data.
     */
    protected function validateData() {

        return request()->validate([
            'name' => 'required',
            'email' => 'sometimes|required|email|unique:users,email,'.request()->id,
            'password' => 'required',
            'club_id' => 'sometimes|required|unique:users,club_id,'.request()->id
        ],
        [
            'club_id.unique' => 'You Choose an already token club.'
        ]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $random = Str::random(4);
        $users = User::all();
        $clubs = Club::all();
        return view('user.index', compact('users', 'random', 'clubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $data = $this->validateData();

        Mail::to($data['email'])->send(new WelcomeMail($data));

        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'club_id' => $data['club_id'],
        ]);

        return redirect()->back()->with('message', $data['name'].' Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $userid)
    {
        $clubs = Club::all();
        return view('user.edit', compact('userid', 'clubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(User $userid)
    {
        $userid->update($this->validateData());

        return redirect('/admin/leaders')->with('message', 'Leader Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $userid)
    {
        $userid->delete();

        return redirect('/admin/leaders')->with('message', 'Leader deleted Successfully');
    }
}
