<?php

namespace App\Http\Controllers;

use App\Request;
use App\User;
use App\Club;

class RequestController extends Controller
{
    /**
     * Validate a listing of the data.
     */
    protected function validateData() {

        return request()->validate([
            'title' => 'required',
            'description' => 'required',
            'priority' => 'required',
            'status' => 'required',
            'user_id' => 'required',
        ]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userid = auth()->user()->id;
        $reqs = Request::where('user_id', $userid)->where('status', 0)->get();
        return view('request.index', compact('reqs'));
    }

    public function adminindex() {
        $reqs = Request::where('status', 0)->get();
        $users = User::all();
        $clubs = Club::all();
        return view('request.admin', compact('reqs', 'users', 'clubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        Request::create($this->validateData());

        return redirect()->back()->with('message', 'Your Request Send Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $reqid)
    {
        return view('request.edit', compact('reqid'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $reqid)
    {
        $reqid->update($this->validateData());

        return redirect('/home/requests')->with('message', 'Request Updated Successfully');
    }

    /**
     * Approve the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function approve(Request $reqid)
    {
        $id = $reqid->id;
        Request::where('id', $id)->update(array('status' => 2));
        return redirect('/admin')->with('message', 'Request Approved Successfully');
    }

    public function notapprove(Request $reqid)
    {
        $id = $reqid->id;
        Request::where('id', $id)->update(array('status' => 1));
        return redirect('/admin')->with('message', 'Request Disapproved Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $reqid)
    {
        $reqid->delete();

        return redirect('/home/requests')->with('message', 'Request Deleted Successfully');
    }
}
